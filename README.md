# Frontend Developer Test

1. I prefer to choose vuejs, because:
	- Clean in syntax, the code is divide into different pieces (template, logic, style).
	- Learning curve is not hard, if you know javascript well.
	- Have a great documentation.
2. Implement stepper / dynamic stepper content (some step and form field is configurable) from vuetify stepper.
3. I think the main focus of product is to get more **User Satisfaction**, eventhough Frontend Developer can build amazing UI, but if the user experience is bad, maybe the user don't want to use the app / product, So Frontend Developer need to know and understand UX to make sure the app / product is usable for the user.
4. Analysis results: 
	- I think better to place "<- Back to Mile" button inside the login box (below the "Not registered yet" text)
	- The text in link "Contact us" maybe it will more relevant if we change the text to "Register here"
	- The others looks good for me
5. Login page link [Mile App Login Clone](https://epic-agnesi-f8fc98.netlify.app/) *maybe it's not better, but I do my best :)
6. Logic problems

6a. Swap the values of variables A and B
```javascript
// Terdapat 2 variable A & B
let A = 3
let B = 5

// Tukar Nilai variabel A & B, syarat tidak boleh menambah variabel baru
// Kedua variabel tersebut dijumlahkan, kemudian dikurangkan.
A = A + B // A => 3 + 5 = 8
B = A - B // B => 8 - 5 = 3
A = A - B // A => 8 - 3 = 5

A = 5
B = 3

// Hasil yang diharapkan
A = 5
B = 3
```
6b. Find the missing numbers from 1 to 100
```javascript
numbers = [1, 2, 4, 5, 6, 7, 8, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100];

const output = missingNumbers(numbers)
console.log(output)

function missingNumbers(array) {
  let missingNumbers = []
  for(let i = 1; i < numbers.length; i++) {
    if(numbers[i] - numbers[i-1] !== 1) {
      missingNumbers.push(numbers[i] - 1)
    }
  }
  return missingNumbers
}

// Output: [3, 9, 41, 60, 71]
```
6c. Return the number which is called more than 1
```javascript
numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 25, 25, 25, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 34, 34, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 83, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 92, 93, 94, 95, 96, 97, 98, 99, 100];

// Function to count occurrence of the numbers element
function countOccurrences(array, searchElement) {
  return array.reduce((accumulator, currentValue) => {
    const occurrence = (currentValue === searchElement) ? 1 : 0;
    return accumulator + occurrence;
  }, 0);
}

// Store element into an array if the occurrence more than 1
let result = []
let count;
for (let number of numbers) {
  count = countOccurrences(numbers, number)
  if (count > 1) {
    result.push(number);
  }
}

// Distinct the element of result array
const distinct = (value, index, self) => {
  return self.indexOf(value) === index;
}
result = result.filter(distinct);

console.log(result)

// Output: [25, 34, 62, 83, 92]
```
6d. Map array element into nested object
```javascript
let array_code = ["1.", "1.1.", "1.2.", "1.3.", "1.4.", "1.1.1.", "1.1.2.", "1.1.3.", "1.2.1.", "1.2.2.", "1.3.1.", "1.3.2.", "1.3.3.", "1.3.4.", "1.4.1.", "1.4.3."];
let array_code_object = array_code.reduceRight((obj, next) => ({[next]: obj}), {});
console.log(array_code_object)
```